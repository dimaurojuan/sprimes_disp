use std::fmt::Display;
use std::fs;
use std::fs::File;
use std::io::{BufReader, Read};
use std::{error, fmt, result};

pub type Result<T> = result::Result<T, SafePrimeError>;

/// ## Errors
///
#[derive(Debug)]
pub struct SafePrimeError {
    e: Option<Box<dyn error::Error>>,
    message: String,
}
impl Display for SafePrimeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Primes error cause by {:?}", self.e)
    }
}

impl error::Error for SafePrimeError {}

impl From<Box<dyn error::Error>> for SafePrimeError {
    fn from(err: Box<dyn error::Error>) -> Self {
        Self {
            e: Some(err),
            message: "".to_string(),
        }
    }
}

impl From<&String> for SafePrimeError {
    fn from(msg: &String) -> Self {
        Self {
            e: None,
            message: msg.to_owned(),
        }
    }
}
impl From<String> for SafePrimeError {
    fn from(msg: String) -> Self {
        Self {
            e: None,
            message: msg.to_owned(),
        }
    }
}

/// ## Trait for getting primes

pub trait SafePrimeDispenser {
    fn get_nth(self, n: u32) -> Result<u32>;
}
/// ## Safe primes source
///
/// This library support reading and storing safe primes of 32 bits in a efficient way.
/// A binary file with the primes is required. The integers are supposed to be encoded
/// in little-endian. Using struct in Python, that means to encode an integer as '<L'
/// Warning: The verification that the binary file has safe primes and only safe primes
/// is not implemented yet. **You should not assume that the outputs of SafePrimesSource are
/// safe primes **
///
/// TODO: Verify that the binary file contains safe primes and only safe primes
///
pub struct SafePrimesSource {
    data: Box<Vec<u32>>,
}
/// ## Auxiliary decoding function
fn dec(buf: [u8; 4]) -> u32 {
    u32::from_le_bytes(buf)
}

impl SafePrimesSource {
    /// Try to create a primes source from a filename where the integers are stored
    pub fn try_from(filename: &String) -> Result<Self> {
        //TODO: Parametrize the size
        //const BYTES_TO_READ: u64 = 4;
        let dec = |buf: [u8; 4]| u32::from_le_bytes(buf);
        let length = fs::metadata(filename)
            .or_else(|e| Err(e.to_string()))?
            .len();

        let file = File::open(filename).or_else(|e| Err(e.to_string()))?;
        let reader = BufReader::new(file);
        // TODO: check if length is a multiple of 4
        //
        let chunks: usize = length as usize / 4;

        let bytes: Vec<u8> = reader
            .bytes()
            .filter(|e| match e {
                Ok(_) => true,
                _ => false,
            })
            .map(|e| e.unwrap())
            .collect();

        let primes: Vec<u32> = (0..chunks)
            .map(|index: usize| {
                let pos = 4 * index;
                let arr: [u8; 4] = [bytes[pos], bytes[pos + 1], bytes[pos + 2], bytes[pos + 3]];
                dec(arr)
            })
            .filter(|x| *x != 0)
            .collect();
        Ok(SafePrimesSource {
            data: Box::new(primes),
        })
    }
}

impl SafePrimeDispenser for SafePrimesSource {
    fn get_nth(self, n: u32) -> Result<u32> {
        match (*self.data).get(n as usize) {
            Some(&prime) => Ok(prime),
            _ => Err(SafePrimeError::from(
                &format!("Couldn't found the prime {}-th", n).to_string(),
            )),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn binary_format() {
        let filename = "primes1000.dat";
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(file);

        let bytes: Vec<u8> = reader
            .bytes()
            .filter(|e| match e {
                Ok(_) => true,
                _ => false,
            })
            .map(|e| e.unwrap())
            .collect();

        let buf = [bytes[0], bytes[1], bytes[2], bytes[3]];

        let n = dec(buf);

        assert_eq!(n, 5);
    }
    #[test]
    fn get_first() {
        let filename = String::from("primes_small.dat");
        let source = SafePrimesSource::try_from(&filename).unwrap();
        let first = source.get_nth(0).unwrap();
        assert_eq!(first, 5);
    }
    #[test]
    fn get_20nth() {
        let filename = String::from("primes_small.dat");
        let source = SafePrimesSource::try_from(&filename).unwrap();
        let first = source.get_nth(20).unwrap();
        assert_eq!(first, 719);
    }
    #[test]
    fn get_first_32bits() {
        let filename = String::from("primes32bits.dat");
        let source = SafePrimesSource::try_from(&filename).unwrap();
        let first = source.get_nth(0).unwrap();
        assert_eq!(first, 2147483783);
    }
    /*
     * #[test]
        fn get_20nth_32bits() {
            let filename = String::from("primes32bits.dat");
            let source = SafePrimesSource::try_from(&filename).unwrap();
            let first = source.get_nth(20).unwrap();
            assert_eq!(first, 719);
        }
    */
}
