# Safe Prime Dispenser

A small crate to provided safe primes stored in the heap. Its goal is to be fast and safe and solve a subtask of several PRG that requires safe primes. This project was born from the need to replace the on-th-fly prime generation (that slows down the runnig time of the algorithms) and replace that by a safe, precomputed table.
 
## Use

  This library support reading and storing safe primes of 32 bits in a efficient way. A binary file with the primes is required. The integers are supposed to be encoded in little-endian. Using struct in Python, that means to encode an integer as `<L`. 

**Warning**: The verification that the binary file has safe primes and only   safe primes
is not implemented yet. **You should not assume that the outputs of Safe  PrimesSource are
safe primes **

TODO: Verify that the binary file contains safe primes and only safe primes

         


